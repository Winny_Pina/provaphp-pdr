<?php
use Phinx\Migration\AbstractMigration;
class Initial extends AbstractMigration
{
    public function up()
    {
        $table = $this->table('atividade');
        $table
            ->addColumn('titulo', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('ordem', 'integer', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->create();

    }
    public function down()
    {
        $this->dropTable('atividade');
    }
}
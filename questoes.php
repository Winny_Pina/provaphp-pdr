<?
/* Q.: 1 */
for ($i = 1; $i <= 100; $i++) {
    if (!($i % 15)) {
        echo "FizzBuzz";
    } else if (!($i % 3)) {
        echo "Fizz";
    } else if (!($i % 5)) {
        echo "Buzz";
    } else {
        echo $i;
    }
}

/* Q.: 2 */
$bIsAuthSession = isset($_SESSION['loggedin']) && ($_SESSION['loggedin'] === true);
$bIsAuthCookie = isset($_COOKIE['Loggedin']) && ($_COOKIE['Loggedin'] === true);

if ($bIsAuthSession || $bIsAuthCookie) {
    header("Location: http://www.google.com");
    exit();
}

/* Q.: 3 */
class MyUserClass
{
    public function getUserList($oConexao = null)
    {
        if (!isset($oConexao) || !($oConexao instanceof DatabaseConnection)) {
            $oConexao = new DatabaseConnection('localhost', 'user', 'password');
        }
        $results = $oConexao->query('select name from user');
        sort($results);
        return $results;
    }
}
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Atividade'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="atividade form large-9 medium-8 columns content">
    <?= $this->Form->create($atividade) ?>
    <fieldset>
        <legend><?= __('Add Atividade') ?></legend>
        <?php
            echo $this->Form->input('titulo');
            echo $this->Form->input('descricao');
            echo $this->Form->input('ordem');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Atividade'), ['action' => 'edit', $atividade->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Atividade'), ['action' => 'delete', $atividade->id], ['confirm' => __('Are you sure you want to delete # {0}?', $atividade->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Atividade'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Atividade'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="atividade view large-9 medium-8 columns content">
    <h3><?= h($atividade->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Titulo') ?></th>
            <td><?= h($atividade->titulo) ?></td>
        </tr>
        <tr>
            <th><?= __('Descricao') ?></th>
            <td><?= h($atividade->descricao) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($atividade->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Ordem') ?></th>
            <td><?= $this->Number->format($atividade->ordem) ?></td>
        </tr>
    </table>
</div>

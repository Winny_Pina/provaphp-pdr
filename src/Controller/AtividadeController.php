<?php
namespace App\Controller;

class AtividadeController extends AppController
{
    public $paginate = [
        'page' => 1,
        'limit' => 10,
        'maxLimit' => 100,
        'fields' => [
            'id', 'titulo', 'descricao', 'ordem'
        ],
        'sortWhitelist' => [
            'id', 'titulo', 'descricao', 'ordem'
        ]
    ];
}
